<?php
declare(strict_types=1);

namespace Charm\Config;

class Error extends \Charm\Error {

    protected $httpStatus = "Internal Configuration Error";

}