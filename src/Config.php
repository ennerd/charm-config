<?php
namespace Charm;

use Charm\Config\Error;

class Config {

    public static function get(string $name, string $default=null) {
        $lower = explode(" ", $name);
        $upper = explode(" ", strtoupper($name));
        $upper_full = implode("_", $upper);

        // constants in the form DATABASE_HOST
        if (defined($upper_full)) {
            return constant($upper_full);
        }

        // constants defined as an array
        if (
            defined($upper[0]) &&
            is_array(constant($upper[0])) &&
            null !== ($val = self::from_array(constant($upper[0]), array_slice($lower, 1)))
        ) {
                return $val;
        }

        // environment variables in the form DATABASE_HOST
        if ($val = getenv($upper_full)) {
            return $val;
        }

        // environment variables in the form DATABASE where DATABASE is a JSON
        if ($val = getenv($upper[0])) {
            try {
                $val = json_decode($val, true, 512, JSON_THROW_ON_ERROR);
                if (null !== ($val = self::from_array($val, array_slice($lower, 1)))) {
                    return $val;
                }
            } catch (\Throwable $e) {}
        }

        // config files in the config/ folder
        $configRoot = Util\ComposerPath::get().'/config';
        $path = $configRoot.'/'.array_shift($lower);

        if (file_exists($path.'.php')) {
            $val = require($path.'.php');
            if (null !== ($val = self::from_array($val, $lower))) {
                return $val;
            }
        }

        if (file_exists($path.'.json')) {
            try {
                $val = json_decode(file_get_contents($path.'.json'), true, 512, JSON_THROW_ON_ERROR);
                if (null !== ($val = self::from_array($val, $lower))) {
                    return $val;
                }
            } catch (\Throwable $e) {}
        }

		if ($default === null) {
            throw new Error("Unknown configuration value '$name'");
		}
    }

    private static function from_array($val, array $keys) {
        if (sizeof($keys) === 0) {
            return $val;
        }
        if (!is_array($val)) {
            return null;
        }
        $with = $val;
        while (sizeof($keys) > 0) {
            $key = array_shift($keys);
            if (!isset($with[$key])) {
                return null;
            }
            $with = $with[$key];
        }
        return $with;
    }

}
