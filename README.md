charm/config
============

An opinionated and simple library to retrieve configuration values for a project.

Usage
-----

Configuration values are identified by string names. 

Example: `Charm\Config::get('database host')` will try to find the value in the following locations:

	1.	The constant `\DATABASE_HOST`.
		Configuration constants are by convention uppercase, so the provided identifier is converted before
		we look for the constant.

	2.	The constant `\DATABASE`.
		If this constant exists and it is a PHP array, we will look for the 'host' key in the array.

	3.  The environment variable `DATABASE_HOST`
        Environment variables follow the same uppercase convention as constants.

	4.  The environment variable `DATABASE`; if found it is JSON-decoded and then we look for the 'host' key.

    5.  The `config/` folder located together with the `composer.json` file; looking for a file named `config/database.php`.
        The file should be a normal PHP file which returns an array.


